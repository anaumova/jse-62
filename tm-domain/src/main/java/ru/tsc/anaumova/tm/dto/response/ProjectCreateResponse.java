package ru.tsc.anaumova.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.dto.model.ProjectDto;

@NoArgsConstructor
public final class ProjectCreateResponse extends AbstractProjectResponse {

    public ProjectCreateResponse(@Nullable final ProjectDto project) {
        super(project);
    }

}