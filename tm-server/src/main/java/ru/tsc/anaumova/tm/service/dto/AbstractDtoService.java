package ru.tsc.anaumova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.anaumova.tm.api.service.dto.IDtoService;
import ru.tsc.anaumova.tm.dto.model.AbstractModelDto;
import ru.tsc.anaumova.tm.repository.dto.AbstractDtoRepository;

import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractDtoService<M extends AbstractModelDto> implements IDtoService<M> {

    @NotNull
    protected abstract AbstractDtoRepository<M> getRepository();

    @Override
    @Transactional
    public void add(@NotNull final M model) {
        @NotNull final AbstractDtoRepository<M> repository = getRepository();
        repository.save(model);
    }

    @Override
    @Transactional
    public void set(@NotNull final Collection<M> models) {
        @NotNull final AbstractDtoRepository<M> repository = getRepository();
        for (final M model : models)
            repository.save(model);
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        @NotNull final AbstractDtoRepository<M> repository = getRepository();
        repository.save(model);
    }

    @Override
    @Transactional
    public void remove(@NotNull final M model) {
        @NotNull final AbstractDtoRepository<M> repository = getRepository();
        repository.delete(model);
    }

    @Override
    @Transactional
    public void clear() {
        @NotNull final AbstractDtoRepository<M> repository = getRepository();
        repository.deleteAll();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final AbstractDtoRepository<M> repository = getRepository();
        return repository.findAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final AbstractDtoRepository<M> repository = getRepository();
        return repository.existsById(id);
    }

    @Override
    public long getCount() {
        @NotNull final AbstractDtoRepository<M> repository = getRepository();
        return repository.count();
    }

}