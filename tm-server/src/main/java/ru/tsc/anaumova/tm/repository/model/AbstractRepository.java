package ru.tsc.anaumova.tm.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.anaumova.tm.model.AbstractModel;

@Repository
public interface AbstractRepository<M extends AbstractModel> extends JpaRepository<M, String> {

}